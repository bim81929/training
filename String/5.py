password = str(input('Enter password: '))
score_lower = 0
score_upper = 0

for i in password:
    if i.lower() == i:
        score_lower += 1
    if i.upper() == i:
        score_upper += 1


print('Lower: ', score_lower)
print('Upper: ', score_upper)