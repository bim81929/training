def docso(num):
    tram = int(num / 100)
    chuc = int(num % 100 / 10)
    donvi = int(num % 10)
    dic = {
        0: 'khong',
        1: 'mot',
        2: 'hai',
        3: 'ba',
        4: 'bon',
        5: 'nam',
        6: 'sau',
        7: 'bay',
        8: 'tam',
        9: 'chin'
    }
    if tram !=0:
        res = dic[tram] + ' tram ' + dic[chuc] + ' muoi ' + dic[donvi]
    if tram == 0 and chuc != 0:
        if chuc != 1:
            res = dic[chuc] + ' muoi ' + dic[donvi]
        else:
            res = ' muoi ' + dic[donvi]
    if tram == 0 and chuc == 0 and donvi != 0:
        res = dic[donvi]
    return res


num = int(input('Nhap so: '))
if num < 1000:
    print(docso(int(num)))
if 100000 > num >= 1000:
    print(docso(int(num / 1000)), 'nghin', docso(int(num % 1000)))
