import sys
from random import randint

l = int(input('Nhap vao do dai: '))
tmp = []
for i in range(0, l):
    tmp.append(randint(1, 100))

print(tmp)
index = int(input('Vi tri chen: '))
value = int(input('Gia tri: '))
if index < 0 or index >= l:
    print('Vi tri khong hop le!')
    sys.exit()
tmp.insert(index, value)
print(tmp)