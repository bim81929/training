import sys
from random import randint

l = int(input('Nhap vao do dai: '))
tmp = []
for i in range(0, l):
    tmp.append(randint(1, 100))

index = int(input('Nhap vao vi tri: '))
if index < 0 or index >= l:
    print('Khong hop le!')
    sys.exit()
else:
    tmp.pop(index)
print(tmp)