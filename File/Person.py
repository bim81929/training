class Person:
    __slots__ = ['name', 'birth', 'sex', 'address']

    def __init__(self, name, birth, sex, address):
        self.name = name
        self.birth = birth
        self.sex = sex
        self.address = address

    def getName(self):
        return self.name

    def getBirth(self):
        return self.birth

    def getSex(self):
        return self.sex

    def getAddress(self):
        return self.address

    def setData(self, name, birth, sex, address):
        self.name = name
        self.birth = birth
        self.sex = sex
        self.address = address

