from Person import Person
import json

data = []
for i in range(0, 2):
    print('Input data #', i + 1, ': ')
    person = Person('', '', '', '')
    name = str(input('Name: '))
    birth = str(input('Birth: '))
    sex = str(input('Sex: '))
    address = str(input('Address: '))
    person.setData(name, birth, sex, address)
    data.append(person)


with open('person.json', 'w') as file:
    json.dump(data, file)